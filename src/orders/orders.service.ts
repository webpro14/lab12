import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const customer = await this.customerRepository.findOneBy({
      id: createOrderDto.customerId,
    });
    console.log(customer);
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.orderRepository.save(order); // ได้ id

    // const orderItems: OrderItem[] = await Promise.all(
    //   createOrderDto.orderItems.map(async (od) => {
    //     const orderIem = new OrderItem();
    //     orderIem.amount = od.amount;
    //     orderIem.product = await this.productRepository.findOneBy({
    //       id: od.productId,
    //     });
    //     orderIem.name = orderIem.product.name;
    //     orderIem.price = orderIem.product.price;
    //     orderIem.total = orderIem.price * orderIem.amount;
    //     orderIem.order = order; // อ้างกลับ
    //     return orderIem;
    //   }),
    // );

    for (const od of createOrderDto.orderItems) {
      const orderIem = new OrderItem();
      orderIem.amount = od.amount;
      orderIem.product = await this.productRepository.findOneBy({
        id: od.productId,
      });
      orderIem.name = orderIem.product.name;
      orderIem.price = orderIem.product.price;
      orderIem.total = orderIem.price * orderIem.amount;
      orderIem.order = order; // อ้างกลับ
      await this.orderItemRepository.save(orderIem);
      order.amount = order.amount + orderIem.amount;
      order.total = order.total + orderIem.amount;
    }

    await this.orderRepository.save(order);
    return await this.orderRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.orderRepository.find({
      relations: ['customer', 'orderItems'],
    });
  }

  findOne(id: number) {
    return this.orderRepository.find({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }
  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.orderRepository.findOneBy({ id: id });
    return this.orderRepository.softRemove(order);
  }
}
